#-*- coding: utf-8 -*-
# Copyright Filip Wasilewski <en@ig.ma>. All rights reserved.

import os
import json

from flask import Flask, url_for, render_template, request, redirect, Response

from geocode import is_lonlat, normalize_address, geocode, get_map_url,\
    decode_key, make_key
import settings

app = Flask(__name__)
app.config.from_object(settings)

UNKNOWN_ADDRESS = u"Nie potrafię znaleźć tego adresu. Czy aby na pewno jest on poprawny?"
INVALID_GEOLINK = u'Ten geolink jest niepoprawny.'


@app.route("/")
def index(template='base.html'):
    q = request.args.get('q', '') or ''
    kwargs = {"q": q}
    if q:
        longitude = latitude = accuracy = None
        if is_lonlat(q):
            longitude, latitude = is_lonlat(q)
            accuracy = 8
        else:
            q = normalize_address(q)
            if q:
                point = geocode(q)
                if point:
                    latitude = float(point['latitude'])
                    longitude = float(point['longitude'])
                    accuracy = point['accuracy'] + 2
                    if accuracy <= 4:
                        accuracy += 1
                    accuracy = min(accuracy, 8)

        if latitude is not None and longitude is not None:
            key = make_key(latitude, longitude, accuracy)
            return redirect(url_for("map", key=key))
        else:
            kwargs['message'] = UNKNOWN_ADDRESS
    return render_template(template, **kwargs)


def get_map_json_response(latitude, longitude, key, bb, **kwargs):
    context = {
        'latitude': latitude, 'longitude': longitude,
        'geohash': key, 'bounding_box': bb,
    }
    return Response(json.dumps(context), mimetype='application/json')


@app.route("/<key>", endpoint="map")
def map_view(key, format=None, template='map.html'):
    data = decode_key(key)

    if not data:
        if format == 'json':
            Response(json.dumps({'error': INVALID_GEOLINK}), status=404)
        else:
            return render_template(template, error=INVALID_GEOLINK, status=404)

    url = request.url
    shorter_url = ''
    if len(url.rsplit('/', 1)[-1]) > 2:
        shorter_url = url[:-1]

    data["url"] = url
    data["shorter_url"] = shorter_url
    data["map_url"] = get_map_url(data["latitude"], data["longitude"], data["bb"])

    if format == 'json':
        return get_map_json_response(**data)

    return render_template(template, show_map=True, **data)


@app.route("/<key>.json")
def map_json(key):
    return map_view(key, format='json')


@app.after_request
def add_header(response):
    """Add header to force latest IE rendering engine and Chrome Frame."""
    response.headers['X-UA-Compatible'] = 'IE=Edge,chrome=1'
    return response


if __name__ == "__main__":
    port = int(os.environ.get("PORT", 5000))
    app.run(host='0.0.0.0', port=port)

