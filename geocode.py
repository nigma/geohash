#-*- coding: utf-8 -*-
# Copyright Filip Wasilewski <en@ig.ma>. All rights reserved.

import json
import urllib
import urllib2
from decimal import Decimal

import settings

GOOGLE_GEOCODE_URI = 'http://maps.google.pl/maps/geo?key=%(key)s&oe=utf8&output=json&sensor=false&q=%(q)s'


def normalize_address(address):
    return u' '.join(address.strip().split())


def geocode_google(query):
    url = GOOGLE_GEOCODE_URI % {
        'key': settings.GOOGLE_JS_API_KEY,
        'q': urllib2.quote(query.encode('utf8'))
    }
    r = urllib2.urlopen(url)
    data = json.load(r)
    r.close()
    data = read_google_data(data)
    return data


def read_google_data(data):
    result = {}
    if data['Status']['code'] == 200 and 'Placemark' in data \
            and len(data['Placemark']) > 0:

        placemark = data['Placemark'][0]
        address_details = placemark['AddressDetails']

        result['status'] = data['Status']['code']
        result['address'] = placemark['address']
        result['accuracy'] = address_details['Accuracy']
        result['longitude'], result['latitude'], result['altitude'] \
            = [Decimal(str(x)) for x in placemark['Point']['coordinates']]

        if 'Country' not in address_details:
            result['country_code'] = None
            result['administrative_area'] = None
            result['locality'] = None
            result['dependent_locality'] = None
            result['thoroughfare'] = None
            result['postal_code'] = None
            return result

        result['country_code'] = address_details['Country']['CountryNameCode']
        administrative_area = address_details['Country'].get('AdministrativeArea', {})
        result['administrative_area'] = administrative_area.get('AdministrativeAreaName')

        locality = None
        try:
            locality = administrative_area['SubAdministrativeArea']['Locality']
        except KeyError:
            try:
                locality = administrative_area['Locality']
            except KeyError:
                # No more interesting data
                pass

        if locality:
            result['locality'] = locality.get('LocalityName')
            result['dependent_locality'] = locality.get('DependentLocalityName')
            result['thoroughfare'] = locality.get('Thoroughfare', {}).get('ThoroughfareName')
            result['postal_code'] = locality.get('PostalCode', {}).get('PostalCodeNumber')
        else:
            result['locality'] = None
            result['dependent_locality'] = None
            result['thoroughfare'] = None
            result['postal_code'] = None
    return result


def geocode(query):
    query = normalize_address(query)
    data = geocode_google(query)
    if data:
        return data


def is_lonlat(locstr):
    if ',' in locstr:
        parts = locstr.split(',')
    else:
        parts = locstr.split()
    if len(parts) == 2:
        try:
            lon, lat = [float(x) for x in parts]
            if -180 < lon < 180 and -90 < lat < 90:
                return lon, lat
        except ValueError:
            pass
    return None


def get_map_url(latitude, longitude, bb, region='PL'):
    return 'http://maps.google.pl/maps/api/staticmap?' + urllib.urlencode({
        'sensor': 'false',
        'size': '500x300',
        'center': "%s,%s" % (latitude, longitude),
        'markers': 'label: |%s,%s' % (latitude, longitude),
        'path': 'fillcolor:0xAA000033|color:0xFFFFFF00|%(n)s,%(e)s|%(n)s,%(w)s|%(s)s,%(w)s|%(s)s,%(e)s' % bb,
        'region': region,
    })


def decode_key(key):
    from geohash import decode_exactly, bbox
    from Geohash.geohash import decode

    try:
        latitude, longitude, latitude_delta, longitude_delta = decode_exactly(key)
        latitude_s, longitude_s = decode(key)
        bb = bbox(key)
    except KeyError:
        return None
    except Exception:
        return None

    if latitude_s.endswith('.'): latitude_s += '0'
    if longitude_s.endswith('.'): longitude_s += '0'

    return dict(
        key=key,
        latitude=latitude, longitude=longitude,
        latitude_delta=latitude_delta, longitude_delta=longitude_delta,
        latitude_s=latitude_s, longitude_s=longitude_s,
        bb=bb
    )


def make_key(latitude, longitude, accuracy):
    from geohash import encode
    return encode(latitude, longitude, accuracy)
